var be = '${ appUrl }/project/Halo/static/';

// Append Style CSS
var ss = document.createElement('link');
ss.type = 'text/css';
ss.rel = 'stylesheet';
ss.href = be + 'style.css';
document.getElementsByTagName('head')[0].appendChild(ss);  

// Append JQuery
var jq = document.createElement('script');  
jq.src = 'https://code.jquery.com/jquery-2.2.3.js';  
// jq.integrity = 'sha256-laXWtGydpwqJ8JA+X9x2miwmaiKhn8tVmOVEigRNtP4=';
jq.crossorigin = 'anonymous';
document.getElementsByTagName('head')[0].appendChild(jq); 
// document.body.appendChild(jq); 

// Append Canvas
var canvas = document.createElement('canvas');
canvas.id = 'mojc';
canvas.className = 'canvas';  
document.body.appendChild(canvas);  
 
// var canvas = document.getElementById("mojc");                
canvas.width = window.innerWidth;
canvas.height = window.innerHeight;

var ctx = canvas.getContext("2d");

        
var paintHalo = function(p1, p2, lw) {
    
    ctx.beginPath();
    ctx.moveTo(p1.x, p1.y);
    ctx.lineTo(p2.x, p2.y);
    ctx.lineWidth = lw; //10
    ctx.lineCap = "square";
    ctx.stroke();
        
    console.log('Paint, from ' + p1.toString() + ' to ' + p2.toString() + ' with ' + lw + ' px');    
};

var paintHalo4 = function(p1, p2, p3, p4, lw) {
        
    ctx.beginPath();
    ctx.moveTo(p1.x, p1.y);
    ctx.lineTo(p2.x, p2.y);
    ctx.lineTo(p3.x, p3.y);
    ctx.lineTo(p4.x, p4.y);
    
    ctx.lineWidth = lw;
    ctx.closePath();
    ctx.stroke();
    
    ctx.fill();
        
    console.log('Paint, points ' + 
        p1.toString() + ', ' +
        p2.toString() + ', ' + 
        p3.toString() + ', ' +  
        p4.toString() + ' with ' + lw + ' px'
    );    
};

function Point(p) {
    this.x = p.x;
    this.y = p.y;    
}

Point.prototype.offset = function offset(point) {
    this.x += point.x;
    this.y += point.y;
    return this;    
}

Point.prototype.transpose = function transpose(factor) {
    this.x = this.x * factor;
    this.y = this.y * factor;
    return this;    
}

Point.prototype.toString = function toString() {    
    return '(' + this.x + ', ' + this.y + ')';    
}


var detectFaceFromUrl = function(imgUrl) {            
    var xmlRequest = jQuery.ajax({
        method: "POST",
        url: "https://exper-api-face.cognitiveservices.azure.com/face/v1.0/detect?returnFaceId=false&returnFaceLandmarks=true",
        contentType: "application/json",
        headers: {
            "Ocp-Apim-Subscription-Key": "8222e5eec7d74f7191cb924e73c795f9"
        },      
        dataType: "json",                  
        data: JSON.stringify({                     
            url: imgUrl 
        })
    });  
    return xmlRequest;  
};           

// $.noConflict();
// jQuery( document ).ready(function() {
    
var main = function() {
        
    // Using jQuery insted of $ to avoid noConflict, itd    
    
/*jQuery('img')
.filter(function() { return this.width >= 150 && this.height >= 150 } )
.sort(function(b, a) { return (a.width*a.height) - (b.width*b.height) } )
.each(function(i, img) { console.log('Slika='+ img.width )} );*/
    
    jQuery("img")
    
    // Filter out imgs that are too small
    .filter(function() { 
        return this.width >= 120 && this.height >= 120 
    })
    
    // Bigger imgs first
    .sort(function(b, a) { 
        return (a.width*a.height) - (b.width*b.height) 
    })
    
    .each(function() {
        
        var img = jQuery(this);                                
        
        // Img absolute position
        var pos = new Point({ 
            x: img.offset().left,
            y: img.offset().top
        });                             
        
        detectFaceFromUrl( 
            img.attr('src') 
        )
        .done(function( data ) {
            
            // Face found
            jQuery.each(data, function(i, face) {
                
                var content = face.faceLandmarks;                 
                                                     
                var eyeLeft = new Point(content.eyeLeftOuter);
                var eyeRight = new Point(content.eyeRightOuter);                                
                var eyeLeftTop = new Point(content.eyeLeftTop);
                var eyeLeftBottom = new Point(content.eyeLeftBottom);                
                var eyeRightTop = new Point(content.eyeRightTop);
                var eyeRightBottom = new Point(content.eyeRightBottom);                   
                
                // If img is resized we must correct points
                // We assume is scaled proportionally                
                var resizedFactor = img.prop('width') / img.prop('naturalWidth');                    
                eyeLeft.transpose(resizedFactor);
                eyeRight.transpose(resizedFactor);
                eyeLeftTop.transpose(resizedFactor);
                eyeLeftBottom.transpose(resizedFactor);
                eyeRightTop.transpose(resizedFactor);
                eyeRightBottom.transpose(resizedFactor);
                
                eyeLeft.offset(pos);
                eyeRight.offset(pos);                                                 
                eyeLeftTop.offset(pos);
                eyeLeftBottom.offset(pos);
                eyeRightTop.offset(pos);
                eyeRightBottom.offset(pos);                
                
                
                console.log(img[0]);
                console.log('Offset point=' + pos.toString());
                // console.log('faceLandmarks=' + JSON.stringify(content));
                console.log('Resize factor=' + resizedFactor);
                
                
                var lineWidth = (eyeRightBottom.y - eyeRightTop.y) * 3.5;                                        
                paintHalo(eyeLeft, eyeRight, lineWidth);
                // paintHalo4(eyeLeftTop, eyeLeftBottom, eyeRightBottom, eyeRightTop, lineWidth);                                                
                
            });      
        });
        
    });

};
// });

function waitForjQuery() {
    
    if (window.jQuery) { 
        main();
        return; 
    } 
    setTimeout(waitForjQuery, 500);
}

waitForjQuery();

/*
jq.onload = function() {
    main();
};    
jq.onerror = function() {
    //Show err    
}; 
jq.src = 'https://code.jquery.com/jquery-2.2.3.js'; 
*/   
