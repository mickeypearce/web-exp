var be = '${ appUrl }/project/Ara/static/';

// Append Firebase DB
var fb = document.createElement('script');  
fb.setAttribute('src', 'https://cdn.firebase.com/js/client/2.2.1/firebase.js');  
document.getElementsByTagName('head')[0].appendChild(fb);

// Append Font CSS
var ss = document.createElement('link');
ss.type = 'text/css';
ss.rel = 'stylesheet';
ss.href = be + 'style.css';
document.getElementsByTagName('head')[0].appendChild(ss);  
  
// Inc exec counter                                                                                            
setTimeout(function(){
    var myDataRef = new Firebase('${ ioUrl }/counter');
    myDataRef.once('value', function(snapshot) {                                        
        var count = (snapshot) ? snapshot.val() + 1 : 0;
        //console.log('snapshot set=' + count);                              
        myDataRef.set(count);                                                                             
    });
}, 1000);

// YouTube audio
// 1. The <iframe> (and video player) will replace this <div> tag.
var youtube = document.createElement('div');
youtube.id = 'player';  
// youtube.style.cssText = 'width: 100%;height: 100%;position: fixed;top: 0;z-index: -1000;transition: opacity 3s;'; 
youtube.className = 'fullscreen fade';
document.body.appendChild(youtube);  
    
// 2. This code loads the IFrame Player API code asynchronously.
var tag = document.createElement('script');

tag.src = 'https://www.youtube.com/iframe_api';
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

// 3. This function creates an <iframe> (and YouTube player)
//    after the API code downloads.
var player;
function onYouTubeIframeAPIReady() {
    player = new YT.Player('player', {
        // height: '390',
        // width: '640',
        videoId: 'ewRjZoRtu0Y',
        events: {
            'onReady': onPlayerReady,
            'onStateChange': onPlayerStateChange
        }
    });
}

// 4. The API will call this function when the video player is ready.
function onPlayerReady(event) {    
    player.setVolume(0);
    playVideo();           
    setTimeout(function(){
        playVideoEnding();;    
    }, 70000)
}

// 5. The API calls this function when the player's state changes.
//    The function indicates that when playing a video (state=1),
//    the player should play for six seconds and then stop.
var done = false;
function onPlayerStateChange(event) {    
    //Loop
    if (event.data == YT.PlayerState.ENDED) {
        // playVideo();
        // done = true;
    } 
    if (event.data == YT.PlayerState.PLAYING && !done) {    
        // I miss promises, we have to set it here so we catch the cycle    
        // fadeIn();
        done = true;
    }   
}
function playVideo() {
    // player.loadVideoById({
    //     'videoId': 'ewRjZoRtu0Y',
    //     // 'startSeconds': 56, //2:47
    //     // 'endSeconds': 78,
    //     'suggestedQuality': 'small' //'large'
    // });        
    player.playVideo(); 
    fadeIn();  
    // player.seekTo(seconds:Number, allowSeekAhead:Boolean):Void  
    setTimeout(function(){
        fadeOut();    
    }, 60000);   
}

function playVideoEnding() {
    player.loadVideoById({
        'videoId': 'r-Nw7HbaeWY',
        'startSeconds': 26, //27
        'endSeconds': 60,
        'suggestedQuality': 'small' //'large'
    });     
    player.playVideo();
    setTimeout(function() {
        fadeInEnding();    
    }, 6000);
       
    setTimeout(function(){
        fadeOutEnding();    
    }, 16000);
}

function fadeIn(){
    //Fade in player
    document.getElementById('player').classList.add('in');    
    // Fade in sound   
    slideVolume({
        from: 0, 
        to: 100, 
        duration: 20000, 
        frequency: 500
    });     
}

function fadeOut(){
    //Fade out player
    document.getElementById('player').classList.remove('in');    
    // Fade out sound   
    slideVolume({
        // from: 0, 
        to: 0, 
        duration: 10000, 
        frequency: 500
    });     
}

function fadeInEnding(){
    //Fade in player
    document.getElementById('player').classList.add('fadefast'); 
    document.getElementById('player').classList.add('in');    
    // Fade in sound   
    slideVolume({
        from: 0, 
        to: 50, 
        duration: 1000, 
        frequency: 1000
    });     
}

function fadeOutEnding(){
    //Fade out player
    document.getElementById('player').classList.remove('in');    
    // Fade out sound   
    slideVolume({
        // from: 0, 
        to: 0, 
        duration: 4000, 
        frequency: 500
    });     
}

function slideVolume(options) {   
    // 0 || 1 => 1
    var from = (typeof options.from !== 'undefined') ?  options.from : player.getVolume(); 
    var step = (options.to - from) * (options.frequency / options.duration); 
    var vol = from;    
    var interval = window.setInterval(function(){                  
            cur_vol = vol; //player.getVolume();
            console.log('cur_vol='+cur_vol);
            if ((cur_vol >= options.to && cur_vol > from) || (cur_vol <= options.to && cur_vol < from)) {
                console.log('abort='+cur_vol);
                clearInterval(interval);
            } else {
                vol += step;    
                player.setVolume(vol);
                console.log('set vol='+vol);      
            }  
    }, options.frequency);
}