const gulp = require("gulp");
const template = require("gulp-template");
const fs = require("fs");

const appName = JSON.parse(fs.readFileSync("./.firebaserc")).projects.default;

gulp.task("build", cb => {
  const appUrl = `//${appName}.firebaseapp.com`;
  const ioUrl = `//${appName}.firebaseio.com`;
  return gulp
    .src("public/**")
    .pipe(
      template({
        appUrl,
        ioUrl
      })
    )
    .pipe(gulp.dest("dist"));
});

gulp.task("build:dev", cb => {
  return gulp
    .src("public/**")
    .pipe(
      template({
        appUrl: "//localhost:5000",
        ioUrl: "//localhost:5000"
      })
    )
    .pipe(gulp.dest("dist"));
});
